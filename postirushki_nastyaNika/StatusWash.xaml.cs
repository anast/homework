﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace postirushki_nastyaNika
{
    /// <summary>
    /// Логика взаимодействия для StatusWash.xaml
    /// </summary>
    public partial class StatusWash : Window
    {
        public StatusWash()
        {
            InitializeComponent();
        }

        private void Btnstatus(object sender, RoutedEventArgs e)
        {
            StatusWash Status = new StatusWash();
            MainWashes Washes = new MainWashes();
            Washes.Show();
            Status.Close();
        }
    }
}
