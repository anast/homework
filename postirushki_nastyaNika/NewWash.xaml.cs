﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace postirushki_nastyaNika
{
    /// <summary>
    /// Логика взаимодействия для NewWash.xaml
    /// </summary>
    public partial class NewWash : Window
    {
        public NewWash()
        {
            InitializeComponent();
        }

        private void Btnnewwash(object sender, RoutedEventArgs e)
        {
            NewWash NewWashes = new NewWash();
            MainWashes Washes = new MainWashes();
            Washes.Show();
            NewWashes.Close();
        }

        private void Btnprint(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Успешно!", "Печать чека");
        }
    }
}
