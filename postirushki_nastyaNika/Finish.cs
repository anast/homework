﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace postirushki_nastyaNika
{
    internal class Finish
    {
            public string Number { get; set; }
            public string WashStyle { get; set; }
            public string Powder { get; set; }
            public string Dry { get; set; }
            public string People { get; set; }
            public string Status { get; set; }
    }
}
